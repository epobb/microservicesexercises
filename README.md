Free source code for [Learn Microservices - ASP.NET Core and Docker](http://leanpub.com/micro) demos and do-it-yourself

# Get the book

Buy the book as [ebook or print](http://leanpub.com/micro).

# Get the source code

Just `git clone` that repository. If that sounds obscure to you, click the "Downloads" link at the left of this window.