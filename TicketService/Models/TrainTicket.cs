﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketService.Models
{
    public class TrainTicket
    {
        public Guid ID { get; set; }
        public string PassengerName { get; set; }

        public Guid TrainScheduleID { get; set; }
    }
}
