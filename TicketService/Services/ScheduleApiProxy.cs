﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TicketService.Models;
using Newtonsoft.Json;
using System.Net.Http;

namespace TicketService.Services
{
    public class ScheduleApiProxy
    {
        const string baseUrl = "http://schedule/api";

        public async Task<ScheduleItem> GetDetailsAsync(Guid id)
        {
            var url = $"{baseUrl}/TrainSchedules/{id}";

            var client = new HttpClient();
            string json = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<ScheduleItem>(json);
        }
    }
}
