﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ScheduleService.Controllers
{
    [Produces("application/json")]
    [Route("api/NextDeparture")]
    public class NextDepartureController : Controller
    {
    public IActionResult GetNext()
    {
        return Ok(new
        {
            DepartureTime = DateTime.Now.AddMinutes(50),
            Number = "5423",
            Destination = "Paris"
        });
    }
    }
}