﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScheduleService.Models;
using ScheduleService.Services;

namespace ScheduleService.Controllers
{
    public class TrainSchedulesViewsController : Controller
    {
        private readonly TrainsDbContext _context;

        public TrainSchedulesViewsController(TrainsDbContext context)
        {
            _context = context;
        }

        // GET: TrainSchedulesViews
        public async Task<IActionResult> Index()
        {
            return View(await _context.Schedules.ToListAsync());
        }

        // GET: TrainSchedulesViews/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainSchedule = await _context.Schedules
                .SingleOrDefaultAsync(m => m.ID == id);
            if (trainSchedule == null)
            {
                return NotFound();
            }

            return View(trainSchedule);
        }

        // GET: TrainSchedulesViews/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TrainSchedulesViews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,DepartureTime,Destination,DistanceKm")] TrainSchedule trainSchedule)
        {
            if (ModelState.IsValid)
            {
                trainSchedule.ID = Guid.NewGuid();
                _context.Add(trainSchedule);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trainSchedule);
        }

        // GET: TrainSchedulesViews/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainSchedule = await _context.Schedules.SingleOrDefaultAsync(m => m.ID == id);
            if (trainSchedule == null)
            {
                return NotFound();
            }
            return View(trainSchedule);
        }

        // POST: TrainSchedulesViews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ID,DepartureTime,Destination,DistanceKm")] TrainSchedule trainSchedule)
        {
            if (id != trainSchedule.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trainSchedule);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrainScheduleExists(trainSchedule.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trainSchedule);
        }

        // GET: TrainSchedulesViews/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainSchedule = await _context.Schedules
                .SingleOrDefaultAsync(m => m.ID == id);
            if (trainSchedule == null)
            {
                return NotFound();
            }

            return View(trainSchedule);
        }

        // POST: TrainSchedulesViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var trainSchedule = await _context.Schedules.SingleOrDefaultAsync(m => m.ID == id);
            _context.Schedules.Remove(trainSchedule);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrainScheduleExists(Guid id)
        {
            return _context.Schedules.Any(e => e.ID == id);
        }
    }
}
