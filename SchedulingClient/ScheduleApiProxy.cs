﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SchedulingClient
{
    public class ScheduleApiProxy
    {
        const string baseUrl = "http://localhost:51426/api";

        public async Task<IEnumerable<ScheduleItem>> GetAllAsync()
        {
            var url = $"{baseUrl}/TrainSchedules";

            var client = new HttpClient();
            string json = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<ScheduleItem>>(json);
        }
    }
}
